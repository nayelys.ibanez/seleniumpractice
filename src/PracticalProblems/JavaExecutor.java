package PracticalProblems;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.JavascriptExecutor;

public class JavaExecutor {
    public static void main(String[] args) throws InterruptedException {
// TODO Auto-generated method stub


        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nayelys.Ibanez\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://ksrtc.in/oprs-web/");


        driver.findElement(By.xpath("//input[@id='fromPlaceName']")).sendKeys("BENG");
        Thread.sleep(2000);

        driver.findElement(By.xpath("//input[@id='fromPlaceName']")).sendKeys(Keys.DOWN);
        System.out.println(driver.findElement(By.xpath("//input[@id='fromPlaceName']")).getText());




        JavascriptExecutor js= (JavascriptExecutor)driver;



        // JavaScript DOM can extract the hidden elements
// because Selenium cannot identify hidden implementation - Ajax implementation
// Investigate the properties of object if it has any hidden text

        String script = "return document.getElementById(\"fromPlaceName\").value;";
        String text = (String) js.executeScript(script);
        System.out.println(text);
        //BENGALURU INTERNATION AIRPORT

        while (!text.equalsIgnoreCase("BENGALURU INTERNATION AIRPORT")){

            driver.findElement(By.xpath("//input[@id='fromPlaceName']")).sendKeys(Keys.DOWN);
            text = (String) js.executeScript(script);
            System.out.println(text);
        }

    }


}
