package PracticalProblems;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TableGrids {

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nayelys.Ibanez\\chromedriver.exe");
        WebDriver driver= new ChromeDriver();
        driver.get("https://www.cricbuzz.com/live-cricket-scorecard/20202/rsa-vs-pak-2nd-odi-pakistan-tour-of-south-africa-2018-19");
        int sum= 0;
        //web element de las tablas presentes
        WebElement table= driver.findElement(By.cssSelector("div[class='cb-col cb-col-100 cb-ltst-wgt-hdr']"));

        // cuenta los tems de cada fila
        int rowcount= table.findElements(By.cssSelector("div[class='cb-col cb-col-100 cb-scrd-itms']")).size();

        // elige especificamente un elemnto de la fila
        int count= table.findElements(By.cssSelector("div[class='cb-col cb-col-100 cb-scrd-itms'] div:nth-child(3)")).size();

        for (int i= 0; i<count-2; i++){
            String value=table.findElements(By.cssSelector("div[class='cb-col cb-col-100 cb-scrd-itms'] div:nth-child(3)")).get(i).getText();
         //convirtiendo el valor obtenido y pasarlo a entero para una suma iterativa de los registros obtenidos
            int valueInteguer= Integer.parseInt(value);
            sum+= valueInteguer;


        }

// obteniendo la ultima parte de lo
        String extra=driver.findElement(By.xpath("//div[text()='Extras']/following-sibling::div")).getText();
        int extraValue= Integer.parseInt(extra);
        int totalSum= sum+ extraValue;
        System.out.println(totalSum);

        String totalValue=driver.findElement(By.xpath("//div[text()='Total']/following-sibling::div")).getText();
        int InteguerTotalValue= Integer.parseInt(totalValue);

        if(totalSum==InteguerTotalValue){
            System.out.println("Count Maches");

        }
        else{
            System.out.println("Count Fails");
        }
    }

}
