package PracticalProblems;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ExerciseTables {

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nayelys.Ibanez\\chromedriver.exe");
        WebDriver driver= new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");

// print count rows
      WebElement rows= driver.findElement(By.cssSelector("table[id='product']"));
      int  countRows=rows.findElements(By.tagName("tr")).size();
        System.out.println(countRows);
// print  count Colums
        int countColumns= driver.findElements(By.xpath("//table[@id='product']//tr/th")).size();
        System.out.println(countColumns);

       int rowsData= driver.findElements(By.cssSelector("tbody tr:nth-child(3) td")).size();
      for (int i= 0; i<rowsData; i++){
       String  ExtracData= driver.findElements(By.cssSelector("tbody tr:nth-child(3) td")).get(i).getText();
          System.out.println(ExtracData);

      }

    }



}
