package PracticalProblems;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ExerciseDropdownAutosuggestive {

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nayelys.Ibanez\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.get("http://qaclickacademy.com/practice.php");

        driver.findElement(By.xpath("//input[@id='autocomplete']")).sendKeys("unit");
        Thread.sleep(2000);
        driver.findElement(By.xpath("//input[@id='autocomplete']")).sendKeys(Keys.DOWN);

        JavascriptExecutor js= (JavascriptExecutor)driver;


        String script = "return document.getElementById(\"autocomplete\").value;";
        String text = (String) js.executeScript(script);
        System.out.println(text);
int i= 0;
        while(!text.equalsIgnoreCase("United States (USA)"))
        {
            i++;
            driver.findElement(By.xpath("//input[@id='autocomplete']")).sendKeys(Keys.DOWN);

            text=(String) js.executeScript(script);
            System.out.println(text);
            if(i>8)
            {
                break;
            }

        }

        if(i>8)
        {
            System.out.println("Element not found");
        }
        else
        {
            System.out.println("Element  found  " + text );
        }



    }
}
