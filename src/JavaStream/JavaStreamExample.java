package JavaStream;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JavaStreamExample { 
//@Test
    public void regular() {
       //count the number of names starting with Alphabet A in list


            ArrayList<String> names = new ArrayList<String>();

            names.add("Abhijeet");
            names.add("Don");
            names.add("Ali");
            names.add("Adam");
            names.add("Aman");
            names.add("Gauri");
            names.add("Munish");
            names.add("Arun");

            int count=0;
            for(int i=0; i<names.size();i++) {
                String actual = names.get(i);
                if(actual.startsWith("A")) {
                    count++;

                }

            }

            System.out.println(count);

        }

@Test
    public void streamFilter(){

    ArrayList<String> names = new ArrayList<String>();

    names.add("Abhijeet");
    names.add("Don");
    names.add("Ali");
    names.add("Adam");
    names.add("Aman");
    names.add("Gauri");
    names.add("Munish");
    names.add("Arun");
    Long n=names.stream().filter(s -> s.startsWith("A")).count();
    System.out.println(n);

    Long d = Stream.of("Abhijeet", "Don", "Ali","Adam", "Ram").filter(s -> {
        s.startsWith("A");
        return true;
    }).count();
    System.out.println(d);

    names.stream().filter(s -> s.length()>4).forEach(s -> System.out.println(s));
    names.stream().filter(s -> s.length()>4).limit(1).forEach(s -> System.out.println(s));

    }


    @Test

    public void streamMap(){

        ArrayList<String> names= new ArrayList<String>();
        names.add("man");
        names.add("Dom");
        names.add("women");


    //imprime los nombres que terminan por a y colocalos en mayuscula
        Stream.of("Abhijeet", "Don", "Ali","Adam", "Ram","Aleya").filter(s -> s.endsWith("a")).map(s -> s.toUpperCase())
                .forEach(s -> System.out.println(s));

        List<String> names1= Arrays.asList("Abhijeet", "Don", "Ali","Adam", "Ram","Aleya");
        names1.stream().filter(s -> s.startsWith("A")).sorted().map(s -> s.toUpperCase()).forEach(s -> System.out.println(s));

        //mezcla dos listas diferentes
        Stream<String> newStream= Stream.concat(names.stream(), names1.stream());
   // newStream.sorted().forEach(s -> System.out.println(s));
        boolean flag= newStream.anyMatch(s -> s.equalsIgnoreCase("Adam"));

        System.out.println(flag);
        Assert.assertTrue(flag);
    }
    @Test
    public void streamCollet(){
        List<String> ls=Stream.of("Abhijeet", "Don", "Ali","Adam", "Ram","Aleya").filter(s -> s.endsWith("a")).map(s -> s.toUpperCase())
                .collect(Collectors.toList());
        System.out.println(ls.get(0));

        //traer miembros unicos de una lista

        List<Integer> values= Arrays.asList(3,2,2,7,5,1,9,7);
        // sort the Array
        values.stream().distinct().forEach(s -> System.out.println(s));
        List<Integer> li=values.stream().distinct().sorted().collect(Collectors.toList());
        System.out.println(li.get(2));

    }



}
