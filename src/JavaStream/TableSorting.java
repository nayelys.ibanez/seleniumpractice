package JavaStream;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.util.List;
import java.util.stream.Collectors;

public class TableSorting {

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nayelys.Ibanez\\chromedriver.exe");
        WebDriver driver= new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://rahulshettyacademy.com/seleniumPractise/#/offers");

        //Click en la columna

       driver.findElement(By.xpath("//th[1]")).click();

        //captura todos los elementos de la lista
        List<WebElement> elements=driver.findElements(By.xpath("//tr/td[1]"));

        //captura el texto de cada elemento
      List <String> originalList=  elements.stream().map(s -> s.getText()).collect(Collectors.toList());
        //obten la lista ordenada

      List <String> sortedList=  originalList.stream().sorted().collect(Collectors.toList());

        //compara ambas listas
        Assert.assertTrue(originalList.equals(sortedList));

        //escanear la lista de elementos con getText y tomar el precios de los beans e imprimirlos
        List<String> price;
     do {
         List<WebElement> rows=driver.findElements(By.xpath("//tr/td[1]"));
          price = rows.stream().filter(s -> s.getText().contains("Rice")).
                 map(s -> getPriceVeggie(s)).collect(Collectors.toList());
         price.forEach(a -> System.out.println(a));

         if (price.size() < 1) {
             driver.findElement(By.cssSelector("a[aria-label='Next']")).click();
         }
     }while (price.size() < 1);


    }

    private static String getPriceVeggie(WebElement s) {
        String priceValue=  s.findElement(By.xpath("following-sibling:: td[1]")).getText();
        return priceValue;
    }
}
