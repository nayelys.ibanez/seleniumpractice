package EndToEnd;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Iterator;
import java.util.Set;

public class Scope {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nayelys.Ibanez\\chromedriver.exe");
        WebDriver driver= new ChromeDriver();
        driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");

        // saber cuantos enlaces tiene una pagina
        System.out.println(driver.findElements(By.tagName("a")).size());

        //tomar una sección de la pagina y saber cuantos enlaces tiene

       WebElement footerDriver= driver.findElement(By.id("gf-BIG"));

        System.out.println(footerDriver.findElements(By.tagName("a")).size());

        // De la sección anterior tomar solo las columna izquierda

       WebElement coloumDriver= driver.findElement(By.xpath("//table/tbody/tr/td[1]/ul"));

        System.out.println(coloumDriver.findElements(By.tagName("a")).size());

 // hacer click en cada link de esta columna

        for (int i= 1; i<coloumDriver.findElements(By.tagName("a")).size(); i++) {

            String clickOnLinkTable = Keys.chord(Keys.CONTROL, Keys.ENTER);

            coloumDriver.findElements(By.tagName("a")).get(i).sendKeys(clickOnLinkTable);
            Thread.sleep(5000);

        }
        // estan abiertas todas las ventanas

            Set<String> abc = driver.getWindowHandles();//4
            Iterator<String> it = abc.iterator();


            while (it.hasNext()){
                driver.switchTo().window(it.next());
                System.out.println(driver.getTitle());
            }

        }
    }

