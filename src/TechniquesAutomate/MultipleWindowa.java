package TechniquesAutomate;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Iterator;
import java.util.Set;

public class MultipleWindowa {
    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nayelys.Ibanez\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://accounts.google.com/signup");


        driver.findElement(By.cssSelector("li:nth-child(1)>a")).click();
        System.out.println("Before switching");
        System.out.println(driver.getTitle());
        Set<String> ids= driver.getWindowHandles();
        Iterator <String>it= ids.iterator();
        String parentid= it.next();
        String childid= it.next();
        driver.switchTo().window(childid);
        System.out.println("After switching");
        System.out.println(driver.getTitle());

        driver.switchTo().window(parentid);
        System.out.println("switching back to parent");
        System.out.println(driver.getTitle());
    }
}
