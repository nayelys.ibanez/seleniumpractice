package TechniquesAutomate;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import javax.swing.*;

public class Frames {

    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nayelys.Ibanez\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.get("https://jqueryui.com/droppable/");

     //   driver.switchTo().frame(1);
// contar cuantos marcos hay en la pagina
        System.out.println(driver.findElements(By.tagName("iframe")).size());

//entrar al marco
        driver.switchTo().frame(driver.findElement(By.cssSelector("iframe[class='demo-frame']")));

        Actions a = new Actions(driver);
        WebElement source= driver.findElement(By.cssSelector("div[id='draggable']"));
        WebElement target=driver.findElement(By.cssSelector("div[id='droppable']"));

        a.dragAndDrop(source, target).build().perform();

        //salir del marco
        driver.switchTo().defaultContent();

    }
}
