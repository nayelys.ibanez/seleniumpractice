package TechniquesAutomate;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import javax.swing.*;

public class actionsDemo {

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nayelys.Ibanez\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.amazon.com/");
        Actions a = new Actions(driver);
        WebElement move= driver.findElement(By.cssSelector("a[id= 'nav-link-accountList']"));

        //moves to specific element, click,press uppercase, send keys
        a.moveToElement(driver.findElement(By.id("twotabsearchtextbox"))).click().keyDown(Keys.SHIFT).sendKeys("hellow").doubleClick().build().perform();

        //moves to specific element
       a.moveToElement(move).build().perform();

    }
}
