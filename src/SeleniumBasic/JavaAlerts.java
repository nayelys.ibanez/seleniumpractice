package SeleniumBasic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class JavaAlerts {

    public static void main(String[] args) throws InterruptedException {
        String text= "Raul";
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nayelys.Ibanez\\chromedriver.exe");
        WebDriver driver= new ChromeDriver();
        driver.get("https://rahulshettyacademy.com/AutomationPractice/");

        driver.findElement(By.id("name")).sendKeys(text);
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("[id='alertbtn']")).click();
        System.out.println(driver.switchTo().alert().getText());
        driver.switchTo().alert().accept(); //metodo para aceptar una ventana emergente de window

        driver.findElement(By.cssSelector("[id=confirmbtn]")).click();
        System.out.println(driver.switchTo().alert().getText());
        driver.switchTo().alert().dismiss();
    }
}
