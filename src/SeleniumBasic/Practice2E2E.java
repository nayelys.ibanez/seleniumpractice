package SeleniumBasic;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class Practice2E2E {

    public static void main(String[] args) {

        ChromeOptions options=new ChromeOptions();

        Map<String, Object> prefs=new HashMap<String,Object>();

        prefs.put("profile.default_content_setting_values.notifications", 1);

//1-Allow, 2-Block, 0-default

        options.setExperimentalOption("prefs",prefs);

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nayelys.Ibanez\\chromedriver.exe");

        WebDriver driver=new ChromeDriver(options);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get("https://www.cleartrip.com/");
        driver.manage().window().maximize();

// TODO Auto-generated method stub


//calendar

            driver.findElement(By.id("DepartDate")).click();
            driver.findElement(By.cssSelector("a.ui-state-default.ui-state-highlight.ui-state-active ")).click();

//DD1

            WebElement adult=driver.findElement(By.id("Adults"));
            Select s =new Select(adult);
            s.selectByIndex(2);

//DD2



            WebElement ch=driver.findElement(By.id("Childrens"));
            Select s1 =new Select(ch);
            s1.selectByIndex(2);



            driver.findElement(By.xpath("//a[@title='More search options']")).click();
            driver.findElement(By.id("AirlineAutocomplete")).sendKeys("indigo");
            driver.findElement(By.id("SearchBtn")).click();

//validate error message

            System.out.println(driver.findElement(By.id("homeErrorMessage")).getText());



        }



    }

