package SeleniumBasic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class DropDown {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nayelys.Ibanez\\chromedriver.exe");
        WebDriver driver= new ChromeDriver();
        driver.get("https://rahulshettyacademy.com/dropdownsPractise/");
//verificar si un checkbox esta tildado, tildardo y verificarlo
        Assert.assertFalse(driver.findElement(By.cssSelector("input[id*='SeniorCitizenDiscount']")).isSelected());
        driver.findElement(By.cssSelector("input[id*='SeniorCitizenDiscount']")).click();
        Assert.assertTrue(driver.findElement(By.cssSelector("input[id*='SeniorCitizenDiscount']")).isSelected());

        // cuenta los checkbox
        System.out.println(driver.findElements(By.cssSelector("input[type='checkbox']")).size());

//Tratamiento de drosown estaticos
      //  Select s =new Select(driver.findElement(By.xpath("//select[@id='ctl00_mainContent_DropDownListCurrency']")));
    //    s.selectByValue("USD");
     //   s.selectByIndex(1);

        //Tratamiento de drpdown donde tengas que colocar la cantidad de adultos a viajar por ejemplo haciendo
        // click en signo + o -

        driver.findElement(By.id("divpaxinfo")).click();
        Thread.sleep(2000L);
       //se necesita hacer click a un objeto 5 veces y se hace un bucle
        int i=1;
        while(i<5){
            driver.findElement(By.id("hrefIncAdt")).click();
            i++;
        }

       driver.findElement(By.id("btnclosepaxoption")).click();
        Assert.assertEquals(driver.findElement(By.id("divpaxinfo")).getText(), "5 Adult");

    }
}
