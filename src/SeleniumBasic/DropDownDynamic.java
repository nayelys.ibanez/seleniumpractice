package SeleniumBasic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DropDownDynamic {
    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nayelys.Ibanez\\chromedriver.exe");
        WebDriver driver= new ChromeDriver();
        driver.get("https://rahulshettyacademy.com/dropdownsPractise/");

        driver.findElement(By.id("ctl00_mainContent_ddl_originStation1_CTXT")).click();
        driver.findElement(By.xpath("//a[@value='BLR']")).click();
        Thread.sleep(2000L);
//para menus dinamicos aparecen y desaparecen elementos es necesario colocar indice del elemento
        driver.findElement(By.xpath("(//a[@value='MAA'])[2]")).click();

        //tambien se puede hacer mediante un xpath de relación padre e hijo
        //// driver.findElement(By.xpath("div[@id='glsctl00_mainContent_ddl_destinationStation1_CTNR'] //a[@value='MAA']").clic()

    }
}
