package SeleniumBasic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class PracticeE2E {

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nayelys.Ibanez\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.cleartrip.com/");
        Thread.sleep(3000L);
        driver.switchTo().alert().dismiss();

        driver.findElement(By.cssSelector("[name='origin']")).sendKeys("new");
        Thread.sleep(3000L);
        List<WebElement> options = driver.findElements(By.cssSelector("li[class='list'] a"));

        for (WebElement option : options) {
            if (option.getText().equalsIgnoreCase("New York, US - John F Kennedy (JFK)")) {
                option.click();
                break;
            }

            driver.findElement(By.cssSelector("[name='destination']")).sendKeys("mia");
            Thread.sleep(3000L);
            List<WebElement> options2 = driver.findElements(By.cssSelector("li[class='list'] a"));

            for (WebElement option2 : options) {
                if (option.getText().equalsIgnoreCase("Miami, US - Miami (MIA)")) {
                    option.click();
                    break;
                }


            }
        }

    }
}
